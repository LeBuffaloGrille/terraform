provider "azurerm" {
  features {}
}

terraform {
  backend "azurerm" {
    resource_group_name = "TFState"
    storage_account_name = "tfstatematteo"
    container_name = "tfstatecontainer"
    key = "test.terraform.tfstate"
    }
}